from urllib import response
import requests
import os
from bs4 import BeautifulSoup
import m3u8ToMp4
from load_m3u8.load.resolve import LoadM3U8


def get_html(url):
  response = requests.get(url)
  if response.status_code == 200:
    return response.text
  else:
    print('request error, status_code:', response.status_code)
    return False
   
def get_content(html, lable='img'):
  soup = BeautifulSoup(html, 'lxml')
  content = soup.find_all(lable)
  # content = soup.find(lable)
  return content

def save_img(dir='default', url_list=[]):
  dir = 'images/' + dir
  if not os.path.exists(dir):
    os.makedirs(dir)
   
  exist_files = os.listdir(dir)
 
  for url in url_list:
    if url.count('https://shp.qpic.cn/'):
      response = requests.get(url, verify=False)
      if response.status_code == 200:
        file_path = dir + '/' + url.split('/')[-2] + '.jpg'
        with open(file_path, 'wb') as f:
          f.write(response.content)
          f.close()
      else:
        print('request error, status_code:', response.status_code)
    elif exist_files.count(url.split('/')[-1]):
      print('file already exists:', url.split('/')[-1])
      continue
    requests.packages.urllib3.disable_warnings()
    response = requests.get(url, verify=False)
    if response.status_code == 200:
      file_path = dir + '/' + url.split('/')[-1]
      with open(file_path, 'wb') as f:
        f.write(response.content)
        f.close()
    else:
      print('request error, status_code:', response.status_code)


def save_m3u8_win(dir='default', url_list=[]):
  tmp_dir = 'videos/' + dir
  if not os.path.exists(tmp_dir):
    os.makedirs(tmp_dir)
  dir = 'C:\\Users\\haotian.shi\\Desktop\\AI\\sese_website\\videos\\' + dir
  i = 0
  for url in url_list:
    try:
      m3u8ToMp4.download(m3u8=url,
                        download_base_path=dir,
                        name=str(i))
    except m3u8ToMp4.exceptions.Error as e:
      print(e)
    finally:
      i += 1

def save_m3u8(dir='default', url_list=[]):
  dir = 'videos/' + dir
  if not os.path.exists(dir):
    os.makedirs(dir)

  exist_files = os.listdir(dir)

  i = 0
  for url in url_list:
    if exist_files.count(str(i)+'.mp4'):
      print('file already exists:', dir+'/'+str(i)+'.mp4')
      continue
    try:
      LoadM3U8(m3u8_url=url, video_path=dir+'/'+str(i)+'.mp4').run()
    except Exception as e:
      print(e)
    finally:
      i += 1


# https://t13.cdn2020.com:12338/video/m3u8/2021/12/06/429cc315/index.m3u8
if __name__ == '__main__':
  for num in range(1000, 2000):
    url = 'http://1162as.oss-cn-beijing.aliyuncs.com/' + str(num) + '.php'
    html = get_html(url)
    if html == False:
      print('current page not exists:', num)
      continue

    print('current page is available:', num)
    img_content = get_content(html, lable='img')
    video_content = get_content(html, lable='video')

    if len(video_content) > 0:
      print('current page has video:', num)
      for video in video_content:
        video_url = video.get('src')
        if video_url == None:
          continue
        print(video_url)
        save_m3u8(dir=str(num), url_list=[video_url])
    elif len(img_content) >= 10:
      print('current page has images:', num)
      for img in img_content:
        img_url = img.get('data-src')
        if img_url == None:
          img_url = img.get('src')
          if img_url == None:
            continue
        print(img_url)
        save_img(dir=str(num), url_list=[img_url])
    else:
      print('current page has ??? ', num)
      continue